$( "td:contains('▲')" ).parent( "tr" ).addClass("base");
$( "td:contains('⁛')" ).parent( "tr" ).addClass("non-binaire");
$( "td:contains('△')" ).parent( "tr" ).addClass("fondue");
$( "td:contains('◌')" ).parent( "tr" ).addClass("diacritique");
$( "td:contains('◊')" ).parent( "tr" ).addClass("acadam");

$( "tr td:nth-child(1)" ).addClass("unicode");
$( "tr td:nth-child(2)" ).addClass("symbole");
$( "tr th:nth-child(2)" ).addClass("symbole");
$(".symbole").hide();
$( "tr td:nth-child(3)" ).addClass("dessin");
$( "tr td:nth-child(4)" ).addClass("nom");
$( "tr td:nth-child(5)" ).addClass("syntaxe");
$( "tr td:nth-child(6)" ).addClass("code");
$( "tr td:nth-child(7)" ).addClass("phrase");
$( "tr td:nth-child(8)" ).addClass("phrase-open-type");
$( "tr td:nth-child(9)" ).addClass("remarque");
$( "tr th:nth-child(9)" ).addClass("remarque");
$( ".remarque" ).hide();

$(".fondue, .base, .non-binaire, .acadam, .diacritique").addClass("color");

$('.base-show').click(function() {
    $(".base").show();
    $(".non-binaire, .fondue, .acadam, .diacritique").hide();
    $(".show-all").removeClass("selectionne-border");
});
$('.non-binaire-show').click(function() {
    $(".non-binaire").show();
    $(".fondue, .base, .acadam, .diacritique").hide();
});
$('.fondue-show').click(function() {
    $(".fondue").show();
    $(".base, .non-binaire, .acadam, .diacritique").hide();
});
$('.diacritique-show').click(function() {
    $(".diacritique").show();
    $(".base, .non-binaire, .fondue, .acadam").hide();
});
$('.acadam-show').click(function() {
    $(".acadam").show();
    $(".base, .non-binaire, .fondue, .diacritique").hide();
});
$('.MEP-show').click(function() {
    $(".unicode, .dessin, .syntaxe").show();
    $(".nom, .code").toggle();
    $(this).toggleClass("selectionne");
});
$(".show-all").addClass("selectionne-border");
$('.show-all').click(function() {
    $(".fondue, .base, .non-binaire, .acadam, .diacritique, .nom, .code").show();
    $(".show-all").addClass("selectionne-border");
});

$('.couleurs-show').click(function() {
    $(".fondue, .base, .non-binaire, .acadam, .diacritique").toggleClass("color");
    $(this).toggleClass("selectionne");
});

$('.baskervvol-show').click(function() {
    $(".syntaxe").toggleClass("BBBaskervvol");
    $(".phrase-open-type").toggleClass("BBBaskervvol");
    $(this).toggleClass("selectionne");
});

$( "td" ).not( ".code" ).css({userSelect: 'none'});
$(".code-select").addClass("selectionne-border");
$('.unicode-select').click(function() {
    $( ".unicode" ).not(".unselected .unicode").css({userSelect: 'text'});
    $( "td" ).not( ".unicode" ).css({userSelect: 'none'});
    $(".code-select").removeClass("selectionne-border");
    $(this).addClass("selectionne-border");
});
$('.code-select').click(function() {
    $( ".code" ).not(".unselected .code").css({userSelect: 'text'});
    $( "td" ).not( ".code" ).css({userSelect: 'none'});
    $(".unicode-select").removeClass("selectionne-border");
    $(this).addClass("selectionne-border");
});

$('td:nth-child(1)').click(function() {
    $(this).parent( "tr" ).toggleClass("unselected");
});

$( ".code" ).not("#cell_F4, #cell_F5, #cell_F6, #cell_F7, #cell_F8, #cell_F9").append( ";" );
