#!usr/bin/env python3
# -*- coding: utf-8 -*-

import fontforge as ff
import os

def qunifying(filename):
    font = ff.open(filename)
    quni_dict = {}

# Convertit la liste en dictionnaire python
# Peut-être faire un pickle de l'objet pour optimiser ?
# lien du calc -> https://lite.framacalc.org/9n4y-bbb-unicode-mapping
    with open('ressources/qunilist', 'r', encoding='utf-8') as qunicode_list:
        for u in qunicode_list.read().splitlines():
            ligne = u.split(',')
            quni_dict[ligne[1]] = (ligne[0], ligne[2])

# Reset la sélection au cas où
    font.selection.none()

    print('-----AJOUT GLYPHES-----')
    for index, lig in enumerate(quni_dict):
        # Vérifie si le glyphe n'existe pas déjà
        if not font.__contains__(lig):
            codepoint = int(quni_dict[lig][0], 16)

            # Construit les glyphes dans la PUA-A
            new_letter = font.createChar(codepoint, lig)
            # new_letter.width = 200

            # Copie chaque glyphe composant la ligature et le colle dans le glyphe fraîchement créé
            for i, gl in enumerate(lig.split('_')):
                if font.__contains__(gl):
                    font.selection.select(gl)
                    font.copy()
                    font.selection.select(lig)
                    # pasteInto() et pas paste() sinon ça supprime ce qui était là avant
                    font.pasteInto()

            new_letter.left_side_bearing = 20
            new_letter.right_side_bearing = 50

#     print('-----GEN OLD_FEATURES-----')
#     try:
#         font.generateFeatureFile('ressources/incomplete.fea')
#     except OSError:
#         print("déso pas de features dans cette fonte")
#         open('ressources/incomplete.fea', 'w')

#     print('-----GEN NEW_FEATURES-----')
#     with open('ressources/base.fea', 'r', encoding='utf-8') as b:
#         base = b.read()
#         base_impri = False

#     with open('ressources/liga.fea', 'r', encoding='utf-8') as l:
#         liga = l.read()
#         liga_impri = False

#     with open('ressources/rlig.fea', 'r', encoding='utf-8') as r:
#         rlig = r.read()
#         rlig_impri = False

#     with open('ressources/new_features.fea', 'w+') as f:
#         with open('ressources/incomplete.fea', 'r') as oldf:
#             contenu = oldf.read().splitlines()

#             for num_ln, ligne in enumerate(contenu):
#                 # print(num_ln+1)
#                 f.write(ligne + '\n')

#                 if num_ln != len(contenu)-1 and '} GDEF;' in contenu[num_ln-1]:
#                     print('---> base.fea')
#                     base_impri = True
#                     base_lines = base.splitlines()
#                     for line in base_lines:
#                         f.write(line + '\n')

#                 if num_ln != len(contenu)-1 and '} liga;' in contenu[num_ln+1]:
#                     print('---> liga.fea')
#                     liga_impri = True
#                     liga_lines = liga.splitlines()
#                     for r in range(3, len(liga_lines)-3):
#                         f.write(liga_lines[r] + '\n')

#                 if num_ln != len(contenu)-1 and  '} rlig;' in contenu[num_ln+1]:
#                     print('---> rlig.fea')
#                     rlig_impri = True
#                     rlig_lines = rlig.splitlines()
#                     for r in range(4, len(rlig_lines)-1):
#                         f.write(rlig_lines[r] + '\n')

#             if not base_impri:
#                 print('---> base.fea')
#                 f.write(base)

#             if not liga_impri:
#                 print('---> liga.fea')
#                 f.write(liga)

#             if not rlig_impri:
#                 print('---> rlig.fea')
#                 f.write(rlig)

    font.mergeFeature('ressources/compil_features.fea')
    font.encoding = 'compacted'
    print('%s-QUNIFIED.%s' % (filename.split('.')[-2], filename.split('.')[-1]))
    font.generate('%s-QUNIFIED.%s' % (filename.split('.')[-2], filename.split('.')[-1]))
    # os.remove('ressources/incomplete.fea')

    print('-----FINI-----')


