<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>✭QUNI EXPRESS✭</title>
	<link rel="stylesheet" href="style.css">
</head>
<body>
	<main>
  <h1>✭DL TA FONTE✭</h1>

  <p><span>{{ file }}</span> a été qunifié·e.</p>
  <p>Téléchargement <a href="{{ file }}-QUNIFIED.{{ ext }}" download>ici</a></p>
  <p>La·e fichier·e de feature généré·e est par <a href="ressources/new_features.fea" download>là</a></p>

	</main>
</body>
</html>
