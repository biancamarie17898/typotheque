# Ce fichier de features documente les prérequis et l’écriture des features OpenType recommandées par la Queer Unicode Initiative de la collective Bye Bye Binary. http://genderfluid.space
# Les contenus de ce fichier sont à mettre dans le fichier features.fea qui se trouve dans le dossier ufo, à la racine
# Pour le rendre fonctionnel dans votre fonte, il faut que cette fonte contienne les 6 glyphes suivantes :
# — un point médian ‘periodcentered’ U+00B7
# — un point médian ajusté à hauteur des capitales — pour cela ajouter un glyphe nommé ‘periodcentered.case’ Hupupup’ tu peux par exemple copier coller ton médian standard et le remonter un poil!
# — un point de suspension aka ‘ellipsis’ U+2026
# — une minuscule latine ē ‘emacron’ U+0113
# — une capitale latine Ē ‘Emacron’ U+0112

# Les ē et Ē macron nous seront utiles pour marquer un rappel de l’accent grave dans les mots avec terminaison en ère comme boulanger·e → boulangēre ou ouvrier·e → ouvriēre.

# Les features fonctionnent en cascade donc l’ordre dans lequel elles sont rédigées a de l’importance.
# Par exemple: on remplace d’abord les points médians en cohabitation avec des capitales par des points médians de capitales ‘periodcentered.case’ avant de lister des remplacement qui font appel à ce même ‘periodcentered.case’ (ex: sub A periodcentered.case E by A_E;)

# Pour les terminaisons en ‘eur/rice’ nous avons opté pour une rédaction en “eur·ice” pour laquelle nous recommandons l’emploi d’une ligature ‘r_i’ U+F4570 (et ‘R_I’ U+F4470).

#♥#♥#

# LE CODE COMMENCE ICI, avec des commentaires avant chaque commande:

# on met toutes les capitales dans une classe:
@Uppercase = [A Acircumflex Adieresis Agrave B C Ccedilla D E Eacute Ecircumflex Edieresis Egrave F G H I Icircumflex Idieresis J K L M N O Ocircumflex Odieresis OE P Q R S T U Ucircumflex Udieresis Ugrave V W X Y Z W_W_W Enb Afem Amasc Cfem Cmasc Efem Emasc Ffem Fmasc Hfem Lfem Lmasc Nfem Omasc Qfem Rfem Rmasc Sfem Smasc Tfem Ufem Umasc Vfem Xmasc A_O A_E F_V C_H C_Q N_N R_S X_C X_S U_L U_R E_S S_L_L L_E L_U];

# on s’assure du bon passage des glyphes inclusifs de bas-de-casse à capitales:
feature case{
    sub a_o by A_O;
    sub a_e by A_E;
    sub f_v by F_V;
    sub c_h by C_H;
    sub c_q by C_Q;
    sub r_s by R_S;
    sub x_c by X_C;
    sub x_s by X_S;
    sub u_l by U_L;
    sub u_r by U_R;
    sub e_s by E_S;
    sub s_l_l by S_L_L;
    sub l_e by L_E;
    sub l_u by L_U;
} case;


# c’est parti pour les ligatures:
feature liga{

  # on s’occupe d’abord des ligatures standards (non-inclusives):
  lookup ligaturesbasiques{
      sub f i by f_i;
      sub f j by f_j;
      sub f l by f_l;
      sub f h by f_h;
      sub f k by f_k;
      sub f f by f_f;
      sub f f i by f_f_i;
      sub f f l by f_f_l;
      } ligaturesbasiques;

  # pour faciliter la saisie sans passer par le point médian, on remplace les .. par des points médians. On cherche donc toutes les suites de 2 points pour les remplacer par un point médian (et les suites de 3 points par une ellipse):
  lookup point {
      sub period period by periodcentered;
      sub period period period by ellipsis;
      } point;

  # on remplace le point médian bas-de-casse par un point médian des capitales ‘periodcentered.case’ quand il est entouré de capitales:
    lookup pointcap {
  	sub @Uppercase periodcentered' by periodcentered.case;
    sub periodcentered' @Uppercase by periodcentered.case;
 } pointcap;


  # on remplace le e ou egrave par un emacron dans les mots boulangēr·e, ouvriēr·e, sēc·he, poēte·sse
  lookup macron {
      sub e' r periodcentered by emacron;
      sub egrave' r periodcentered by emacron;
      sub E' R periodcentered.case by Emacron;
      sub Egrave' R periodcentered.case by Emacron;
    	sub e' c periodcentered h by emacron;
    	sub egrave' c periodcentered h by emacron;
    	sub E' C periodcentered.case H by Emacron;
    	sub Egrave' C periodcentered.case H by Emacron;
    	sub eacute' t e periodcentered s s by emacron;
    	sub egrave' t e periodcentered s s by emacron;
    	sub Eacute' T E periodcentered.case S S by Emacron;
    	sub Egrave' T E periodcentered.case S S by Emacron;
      } macron;

  # AJOUTEZ VOS LIGATURES INCLUSIVES ICI ♥ ♥ : (voir en haut pour l'ordre/cascade et le lien vers la syntaxe/tableau QUNI)
  lookup ligaturesinclusives {
      sub a periodcentered e by a_e;
      sub e periodcentered a by e_a;
      sub a periodcentered o by a_o;
      sub c periodcentered h by c_h;
      sub c periodcentered q by c_q;
      sub f periodcentered v by f_v;
      sub l e s periodcentered e by l_e;
      sub l e periodcentered l u by l_u;
      sub r periodcentered s by r_s;
      sub l l e periodcentered by s_l_l;
      sub periodcentered s s by s_s;
      sub u periodcentered l by u_l;
      sub x periodcentered c by x_c;
      sub x periodcentered s by x_s;
      sub A periodcentered.case E by A_E;
      sub E periodcentered.case A by E_A;
      sub A periodcentered.case O by A_O;
      sub C periodcentered.case H by C_H;
      sub C periodcentered.case Q by C_Q;
      sub F periodcentered.case V by F_V;
      sub L E S periodcentered.case E by L_E;
      sub L E periodcentered.case L U by L_U;
      sub N periodcentered.case N by N_N;
      sub R periodcentered.case S by R_S;
      sub L L E periodcentered.case by S_L_L;
      sub periodcentered.case S S by S_S;
      sub U periodcentered.case L by U_L;
      sub U R periodcentered.case R by U_R;
      sub X periodcentered.case C by X_C;
      sub X periodcentered.case S by X_S;
      } ligaturesinclusives;

} liga;


#On duplique tout le contenu de "liga" dans "rlig" (sauf les ligatures basiques) en renommant les lookups (par exemple ajouter un "2" à chaque fois).
#Cela permet aux ligatures de ne pas sauter lorsqu'on augmente l'interlettrage dans un logiciel de mise en pages. Il est nécessaire que les lignes soient présentes à la fois dans "liga" et "rlig" car tous les logiciels ne supportent pas "rlig" (mais ils supportent tous "liga").
feature rlig{

# pour faciliter la saisie sans passer par le point médian, on remplace les .. par des points médians. On cherche donc toutes les suites de 2 points pour les remplacer par un point médian (et les suites de 3 points par une ellipse):
  lookup point2 {
      sub period period by periodcentered;
      sub period period period by ellipsis;
      } point2;

  # on remplace le point médian bas-de-casse par un point médian des capitales ‘periodcentered.case’ quand il est entouré de capitales:
  	sub @Uppercase periodcentered' by periodcentered.case;
      sub periodcentered' @Uppercase by periodcentered.case;

  # on remplace le e ou egrave par un emacron dans les mots boulangēr·e, ouvriēr·e, sēc·he, poēte·sse
  lookup macron2 {
      sub e' r periodcentered by emacron;
      sub egrave' r periodcentered by emacron;
      sub E' R periodcentered.case by Emacron;
      sub Egrave' R periodcentered.case by Emacron;
    	sub e' c periodcentered h by emacron;
    	sub egrave' c periodcentered h by emacron;
    	sub E' C periodcentered.case H by Emacron;
    	sub Egrave' C periodcentered.case H by Emacron;
    	sub eacute' t e periodcentered s s by emacron;
    	sub egrave' t e periodcentered s s by emacron;
    	sub Eacute' T E periodcentered.case S S by Emacron;
    	sub Egrave' T E periodcentered.case S S by Emacron;
      } macron2;

  # on applique toutes les ligatures inclusives aux endroits où il y a un point médian:
  lookup ligaturesinclusives2 {
      sub a periodcentered e by a_e;
      sub e periodcentered a by e_a;
      sub a periodcentered o by a_o;
      sub c periodcentered h by c_h;
      sub c periodcentered q by c_q;
      sub f periodcentered v by f_v;
      sub l e s periodcentered e by l_e;
      sub l e periodcentered l u by l_u;
      sub r periodcentered s by r_s;
      sub l l e periodcentered by s_l_l;
      sub periodcentered s s by s_s;
      sub u periodcentered l by u_l;
      sub x periodcentered c by x_c;
      sub x periodcentered s by x_s;
      sub A periodcentered.case E by A_E;
      sub E periodcentered.case A by E_A;
      sub A periodcentered.case O by A_O;
      sub C periodcentered.case H by C_H;
      sub C periodcentered.case Q by C_Q;
      sub F periodcentered.case V by F_V;
      sub L E S periodcentered.case E by L_E;
      sub L E periodcentered.case L U by L_U;
      sub N periodcentered.case N by N_N;
      sub R periodcentered.case S by R_S;
      sub L L E periodcentered.case by S_L_L;
      sub periodcentered.case S S by S_S;
      sub U periodcentered.case L by U_L;
      sub U R periodcentered.case R by U_R;
      sub X periodcentered.case C by X_C;
      sub X periodcentered.case S by X_S;
      } ligaturesinclusives2;

} rlig;
